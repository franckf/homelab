# [homelab](https://franckf.gitlab.io/homelab/README.md)

documentation et pensées diverses sur la mise en place et l'utilisation d'un environnement de qualification personnel.

[go to wiki](https://gitlab.com/franckf/homelab/-/wikis/home)
